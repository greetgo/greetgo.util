package kz.greetgo.util.fui.handler;

public class BoolChangeHandlerList extends HandlerList<BoolChangeHandler> {
  public BoolChangeHandlerList(ThrowAcceptor throwAcceptor) {
    super(throwAcceptor);
  }

  public void fire(boolean boolValue) {
    for (final BoolChangeHandler handler : this) {
      try {
        handler.changed(boolValue);
      } catch (Throwable e) {
        getThrowAcceptor().accept(e);
      }
    }
  }
}
