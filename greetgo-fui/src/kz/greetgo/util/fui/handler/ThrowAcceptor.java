package kz.greetgo.util.fui.handler;

public interface ThrowAcceptor {
  void accept(Throwable e);
}
