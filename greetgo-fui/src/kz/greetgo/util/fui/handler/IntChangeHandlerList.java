package kz.greetgo.util.fui.handler;

public class IntChangeHandlerList extends HandlerList<IntChangeHandler> {
  public IntChangeHandlerList(ThrowAcceptor throwAcceptor) {
    super(throwAcceptor);
  }

  public void fire(int intValue) {
    for (final IntChangeHandler handler : this) {
      try {
        handler.changed(intValue);
      } catch (Throwable e) {
        getThrowAcceptor().accept(e);
      }
    }
  }

  public final StrChangeHandler strChangeHandler = strValue -> {
    try {
      fire(strValue == null ? 0 : Integer.parseInt(strValue.trim()));
    } catch (NumberFormatException e) {
      fire(0);
    }
  };
}
