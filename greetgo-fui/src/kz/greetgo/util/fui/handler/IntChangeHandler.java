package kz.greetgo.util.fui.handler;

public interface IntChangeHandler {
  void changed(int intValue) throws Throwable;
}
