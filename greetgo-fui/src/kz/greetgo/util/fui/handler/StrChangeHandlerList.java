package kz.greetgo.util.fui.handler;

public class StrChangeHandlerList extends HandlerList<StrChangeHandler> {

  public StrChangeHandlerList(ThrowAcceptor throwAcceptor) {
    super(throwAcceptor);
  }

  public void fire(String strValue) {
    for (final StrChangeHandler handler : this) {
      try {
        handler.changed(strValue);
      } catch (Throwable e) {
        getThrowAcceptor().accept(e);
      }
    }
  }
}
