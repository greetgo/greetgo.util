package kz.greetgo.util.fui.handler;

public class ButtonClickHandlerList extends HandlerList<ButtonClickHandler> {
  public ButtonClickHandlerList(ThrowAcceptor throwAcceptor) {
    super(throwAcceptor);
  }

  public void fire() {
    for (final ButtonClickHandler handler : this) {
      try {
        handler.clicked();
      } catch (Throwable e) {
        getThrowAcceptor().accept(e);
      }
    }
  }

}
